# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
#
#   jorgescalona@riseup.net   @jorgemustaine  https://github.com/jorgescalona
#
##############################################################################
{
    "name" : "openacademy jorge",
    "version" : "0.0.2",
    "author" : "jorgescalona @jorgemustaine",
    "website" : "www.attakatara.wordpress.com",
    "category" : "Desconocida",
    "description" : """este módulo es de fines didacticos siguiendo
                       lo explicado en www.odoo.com/documentation/9.0/howtos/backend.html""",
    "depends" : ['base', 'board', 'l10n_ve_dpt'],
    "data" : [ 'security/security.xml',
               'security/ir.model.access.csv',
               'templates.xml',
               'views/openacademy.xml',
               'views/partner.xml',
               'views/session_workflow.xml',
               'views/session_board.xml',
               'reports/report_session.xml',
               ],
    "init_xml" : [],
    "demo" : [#solo se carga en el modo demostración
              'demo.xml',],
    "installable" : True,
}

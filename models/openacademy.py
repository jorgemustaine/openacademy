# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
#
#   jorgescalona@riseup.net   @jorgemustaine  https://github.com/jorgescalona
#
##############################################################################

from datetime import timedelta
from openerp import models, fields, api, exceptions

class Course(models.Model):
    """crea el modelo Course y define sus decoradores y restricciones """
    _name = 'openacademy.course'
    name = fields.Char(string="nombre del curso: ", required=True)
    description = fields.Text()
    responsible_id = fields.Many2one('res.users', ondelete='set null', string='Responsable', index=True)
    session_ids = fields.One2many('openacademy.session', 'course_id', string="Sesiones")
    # lo siguiente permite duplicar el Course object cambiando su nombre original
    # en Copy of[original name]
    @api.multi
    def copy(self, default=None):
        default = dict(default or {})

        copied_count = self.search_count(
             [('name', '=like', u"Copy of {}%".format(self.name))])
        if not copied_count:
            new_name = u"Copy of {}".format(self.name)
        else:
            new_name = u"Copy of {} ({})".format(self.name, copied_count)

        default['name'] = new_name
        return super(Course, self).copy(default)

    # Se aqgregan los SQL constraints más info https://www.postgresql.org/docs/8.1/static/ddl-constraints.html
    _sql_constraints = [
        ('name_description_check',
         'CHECK(name != description)',
         "El titulo del Curso y la descripción del mismo no pueden ser iguales"),

        ('name_unique',
         'UNIQUE(name)',
         "El nombre del Curso debe ser único"),
    ]

class Session(models.Model):
    """define el model Session para interpretación del ORM"""
    _name = 'openacademy.session'

    name = fields.Char(required=True)
    start_date = fields.Date(default=fields.Date.today, string="Fecha de Inicio")
    duration = fields.Float(digits=(6, 2), help="Duración en días ")
    seats = fields.Integer(string="Número de vacantes ")
    active = fields.Boolean(default=True)
    color = fields.Integer()
    # se modifica el filtro domain agregando al principio el operador lógico OR '|'
    # además se usa el operador ilike que verifica registros parecidos a Maestros
    # ejemplo: [('name', 'like', 'maestro')] localiza los registros con nombres como:
    # 'maestro', 'maestros', 'remaestro'... Pero No 'Maestro'
    # [('name', '=like', 'maestro')] este encuentra todos los registros 'maestro'
    # [('name', 'ilike', 'maestro')] este es el más universal de las busquedas y encuentra:
    # 'maestro', 'MAESTRO', 'Maestros', 'MaEsTroS', 'remestros' .......
    # [('name', '=ilike', 'maestro')] encuentra registros: 'maestro', 'Maestro', 'MAESTRO', MaEsTrO'
    instructor_id = fields.Many2one('res.partner', string='Instructor',
                                     domain=['|',('instructor','=','True'),
                                             ('category_id.name', 'ilike', "Maestro")])
    course_id = fields.Many2one('openacademy.course', ondelete='cascade', string='Curso', required=True)
    attendee_ids = fields.Many2many('res.partner', string="Alumnos")
    taken_seats = fields.Float(string="Cupos Tomados", compute='_taken_seats')
    end_date = fields.Date(string="Fecha de Culminación", store=True,
                           compute='_get_end_date', inverse='_set_end_date')
    hours = fields.Float(string="Duración en horas", compute='_get_hours',
                         inverse='_set_hours')
    # en el siguiente campo de cálculo se usa para plasmar en la vista gráfico
    # es por ello que se usa el atributo store=True
    attendees_count = fields.Integer(string="Attendees count", compute='_get_attendees_count', store=True)
    state = fields.Selection([('draft', "Borrador"),
                              ('confirmed', "Confirmado"),
                              ('done', "Hecho")
                              ], readonly=True)

    @api.multi
    def action_draft(self):
        self.state = 'draft'

    @api.multi
    def action_confirm(self):
        self.state = 'confirmed'

    @api.multi
    def action_done(self):
        self.state = 'done'

    @api.depends('seats', 'attendee_ids')
    def _taken_seats(self):
        for r in self:
            if not r.seats:
                r.taken_seats = 0.0
            else:
                r.taken_seats = 100.0 * len(r.attendee_ids) / r.seats

    @api.onchange('seats', 'attendee_ids')
    def _verify_valid_seats(self):
        if self.seats < 0:
            return {
                'warning':{
                    'title' : 'Valor Incorrepto de Cupos',
                    'message' : 'El número de cupos disponibles no puede ser un entero negativo',
                }
            }
        if self.seats < len(self.attendee_ids):
            return {
                'warning' : {
                    'title' : 'Número Incorrecto de cupos',
                    'message' : 'Excede los cupos, incremente los cupos o remueva participantes',
                }
            }

    @api.depends('start_date', 'duration')
    def _get_end_date(self):
        """esta función obtiene el día de culminación del curso"""
        for r in self:
            if not (r.start_date and r.duration):
                r.end_date = r.start_date
                continue
            # Agrega duración hasta start_date, pero: Lunes + 5 días = sábado
            # por lo que se resta un segundo para obtener viernes
            start = fields.Datetime.from_string(r.start_date)
            duration = timedelta(days=r.duration, seconds=-1)
            r.end_date = start + duration

    def _set_end_date(self):
        """Calcula el día final del Curso"""
        for r in self:
            if not (r.start_date and r.end_date):
                continue

            # Calcula la diferencia entre fechas, pero: viernes - lunes = 4 días,
            # por eso se agrega un día para obtener 5
            start_date = fields.Datetime.from_string(r.start_date)
            end_date = fields.Datetime.from_string(r.end_date)
            r.duration = (end_date - start_date).days + 1

    @api.depends('duration')
    def _get_hours(self):
        for r in self:
            r.hours = r.duration * 24

    def _set_hours(self):
        for r in self:
            r.duration = r.hours / 24

    @api.depends('attendee_ids')
    def _get_attendees_count(self):
        """calcula el total de inscritos en un curso"""
        for r in self:
            r.attendees_count = len(r.attendee_ids)

    @api.constrains('instructor_id', 'attendee_ids')
    def _check_instructor_not_in_attendees(self):
        """Función valida que el instructor no sea alumno de su clase"""
        for r in self:
            if r.instructor_id and r.instructor_id in r.attendee_ids:
                raise exceptions.ValidationError('Un instructor no puede ser alumno de su propia clase')



# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
#
#   jorgescalona@riseup.net   @jorgemustaine  https://github.com/jorgescalona
#
##############################################################################
from openerp import fields, models

class Partner(models.Model):
    """esta clase crea el modelo hijo de res.partner"""
    _inherit = "res.partner"

    # Agrega una nueva columna a res.partner, por defecto los partners no son instructores
    instructor = fields.Boolean("Instructor", default=False)
    session_ids = fields.Many2many('openacademy.session', string="Atención de Sesión", readonly=True)
    estado_id = fields.Many2one('estado', 'Estado', help='entidad federal originaria del alumno')
    municipio_id = fields.Many2one('municipio', 'Municipio', help='municipio de la ubicacion geografica')
    parroquia_id = fields.Many2one('parroquia', 'Parroquia', help='parroquia de la ubicación geográfica')

    def pulsar_estado(self, cr, ids, context=None):
        """ esta función se encarga de limpiar los campos de mun y parr cuando se pulse estado """
        return {'value':{'municipio_id':'','parroquia_id':''}}
    def pulsar_municipio(self, cr, ids, context=None):
        """ esta función se encarga de limpiar el campo de parr cuandose pulse municipio """
        return {'value':{'parroquia_id':''}}


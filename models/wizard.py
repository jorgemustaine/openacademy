# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
#
#   jorgescalona@riseup.net   @jorgemustaine  https://github.com/jorgescalona
#
##############################################################################

from openerp import models, fields, api

class Wizard(models.TransientModel):
    """clase que genera un asistente y hereda su comportamiento de
       models pero de forma transitoria"""
    _name = 'openacademy.wizard'

    def _default_session(self):
        return self.env['openacademy.session'].browse(self._context.get('active_id'))

    session_id = fields.Many2one('openacademy.session',
                                 'string="Seciones o Clases"',
                                 required=True,
                                 default=_default_session)
    attendee_ids = fields.Many2many('res.partner', string="Asistentes")

    @api.multi
    def subscribe(self):
        """metodo para agregar attendees a las clases desde el
           button en el wizard"""
        self.session_id.attendee_ids |= self.attendee_ids
        return {}

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import functools
import xmlrpclib

HOST = 'localhost'
PORT = 8069
DB = 'openacademy'
USER = 'admin'
PASS = 'admin'
ROOT = 'http://%s:%d/xmlrpc/' % (HOST,PORT)

# login

uid = xmlrpclib.ServerProxy(ROOT + 'common').login(DB,USER,PASS)
print "Logueando como %s (uid:%d)" % (USER,uid)

call = functools.partial(xmlrpclib.ServerProxy(ROOT + 'object').execute, DB, uid, PASS)

# Leer las Sesiones
sessions = call('openacademy.session', 'search_read', [], ['name', 'seats'])
for session in sessions:
	print "Clase %s (%s cupos)" % (session['name'], session['seats'])

# Crear una nueva clase
session_id = call('openacademy.session', 'create', {
	'name' : 'Mi Clase',
	'course_id' : 15,
})
